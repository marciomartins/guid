{
Perguntar ao Ronaldo pq usou TcustomForm ao inv�s de TForm;

}
unit Vcl.InputForms;

interface

uses Classes, System.TypInfo, Vcl.InputDialog, Vcl.Forms, Vcl.Controls,
     Vcl.ExtCtrls, Vcl.StdCtrls, RTTI;

type
  TVisualDialog = class(TCustomForm)
  strict private
    Dialog: Idialog;
  private
    function CreateControlFor(AField: TField; AParent: TWinControl): TControl;
    procedure FlowAlign;
    procedure OnFieldExit(Sender: TObject);
  public
    function Execute(ADialog: IDialog): Boolean;
    constructor Create(AOwner: TComponent); override;
  end;

implementation

procedure ExecuteVisualDialog(ADialog: IDialog);
var
  Form: TVisualDialog;
begin
  Form := TVisualDialog.Create(nil);
  try
    Form.Execute(ADialog);
  finally
    Form.Free;
  end;
end;

procedure SaveVisualDialog(AField: IField);
begin
  case Afield.GetKind of
    tkString: string(AField.GetReference^) := (AField as TEdit).Text;
  end;
end;

{ TVisualDialog }

constructor TVisualDialog.Create(AOwner: TComponent);
begin
  inherited CreateNew(AOwner);
  //todo: Implementar ControlesCriados
  //ControlesCriados := TObjectList.Create;
  BorderStyle := bsDialog;
  Position := poScreenCenter;
  KeyPreview := True;
  AutoScroll := False;
  Constraints.MaxHeight := Monitor.WorkareaRect.Bottom - Monitor.WorkareaRect.Top - 20;
  //TODO: Implementar os eventos FormKeyDown e FormKeyPress
  //OnKeyDown := FormKeyDown;
  //OnKeyPress := FormKeyPress;
end;

procedure TVisualDialog.FlowAlign;
var
  I, X, Y: Integer;
  C: TControl;
begin
  X := 0;
  Y := 0;
  for I := 0 to ControlCount - 1 do
  begin
    C := Controls[I];
    if (X > 0) and (X + C.Width > ClientWidth) then
    begin
      Inc(Y, C.Height);
      X := 0;
    end;
    C.Left := X;
    C.Top := Y;
    Inc(X, C.Width);
  end;
end;

procedure TVisualDialog.OnFieldExit(Sender: TObject);
begin
  if Sender is TField then
  begin
    TField(Sender).Save;
  end;
end;

function TVisualDialog.Execute(ADialog: IDialog): Boolean;
var
  Field: TField;
begin
  Self.Dialog := ADialog;
  Self.Caption := ADialog.GetWindowTitle;
  //Todo: Implementar o CriaControle
  for Field in ADialog.Fields do
  begin
    CreateControlFor(Field, Self);
  end;
  FlowAlign;
  //Todo: Implementar o Recarrega;
  Result := ShowModal = mrOk;
end;

function TVisualDialog.CreateControlFor(AField: TField; AParent: TWinControl): TControl;
var
  Panel: TPanel;
  Lab: TLabel;
  Edit: TEdit;
begin
  Panel := TPanel.Create(Self);
  Lab := TLabel.Create(Self);
  Edit := TEdit.Create(Self);
  Lab.Parent := Panel;
  Edit.Parent := Panel;
  Panel.Parent := AParent;
  Lab.Left := 0;
  Lab.Top := 0;
  Lab.Caption := AField.Title;
  Edit.Left := 0;
  Edit.Top := Lab.Height + 4;
  Edit.OnExit := OnFieldExit;
  Panel.AutoSize := True;
  // carregar
  case AField.Kind of
    tkString, tkWString, tkUString: ;//Edit.Text := string(AField.Reference^);
  end;
  Result := Panel;
end;


initialization
  DialogExecuteProc := ExecuteVisualDialog;
  DialogSaveProc := SaveVisualDialog;

finalization
  DialogExecuteProc := nil;
  DialogSaveProc := nil;

end.
