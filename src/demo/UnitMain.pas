unit UnitMain;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.InputDialog,
  Vcl.InputForms, RTTI, TypInfo, Data.Bind.EngExt, Vcl.Bind.DBEngExt,
  System.Bindings.Outputs, Vcl.Bind.Editors, Data.Bind.Components;

type
  TForm3 = class(TForm)
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    Label1: TLabel;
    Edit1: TEdit;
    BindingsList1: TBindingsList;
    Edit2: TEdit;
    procedure Button1Click(Sender: TObject);
    procedure Edit1Change(Sender: TObject);
  private
    { Private declarations }
  public
  end;

var
  Form3: TForm3;

implementation

uses UPeople;

{$R *.dfm}

procedure TForm3.Button1Click(Sender: TObject);
var
  P: TPeople;
begin
  P := TPeople.Create;
  try
    Dialog('Primeiro Teste').Execute;
    //Exemplo de como gostaria de ser usado
    //Dialog(P).Execute;
    //ShowMessage(P.Name);
  finally
    P.Free;
  end;
end;

procedure TForm3.Edit1Change(Sender: TObject);
begin
  BindingsList1.Notify(Sender, '');
end;

end.
