unit UPeople;

interface

type
  [Title('Pessoa')]
  TPeople = class
  private
    FName: string;
    procedure SetName(const Value: string);
  published
    [Required, MaxLenght(30), Display('Nome')]
    property Name: string read FName write SetName;
  public
  end;

implementation

{ TPeople }

procedure TPeople.SetName(const Value: string);
begin
  FName := Value;
end;

end.
