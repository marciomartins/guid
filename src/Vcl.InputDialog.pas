unit Vcl.InputDialog;

interface

uses Vcl.Dialogs, System.SysUtils, Generics.Collections, System.TypInfo;

type
  TField = class;

  IField = interface
    ['{5FAB7815-8FE8-4DCC-BD21-5FFACE6B091A}']
    function GetKind: TTypeKind;
    function GetReference: Pointer;
  end;

  TField = class(TInterfacedObject, IField)
  public
    Title: string;
    Reference: Pointer;
    Kind: TTypeKind;
    Length: Integer;
    procedure Save;
    function GetKind: TTypeKind;
    function GetReference: Pointer;
  end;

  IDialog = interface
    ['{5A167C7F-98E8-4812-B7F2-7791C13F107A}']
    procedure SetWindowTitle(const ATitle: string);
    function GetWindowTitle: string;
    function Fields: TObjectList<TField>;
  end;

  IDialogDesigner = interface
    ['{C290D917-71C1-4A84-9A7D-11295D768D48}']
    procedure Execute;
  end;

  TDialog = class(TInterfacedObject, IDialogDesigner, IDialog)
    FWindowTitle: string;
  private
    FFields: TObjectList<TField>;
    procedure VisualRequired;
  public
    constructor Create;
    destructor Destroy; override;
    function GetWindowTitle: string;
    procedure SetWindowTitle(const ATitle: string);
    procedure Execute;
    function Fields: TObjectList<TField>;
  end;

var
  DialogExecuteProc: procedure(ADialog: IDialog) = nil;
  DialogSaveProc: procedure(AField: IField) = nil;

function Dialog(const ATitle: string): IDialogDesigner;
//Dialog('Titulo da Janela').Execute;

implementation

function Dialog(const ATitle: string): IDialogDesigner;
var
  Dialog: TDialog;
begin
  Dialog := TDialog.Create;
  Dialog.SetWindowTitle(ATitle);
  //Depois implementa o que falta
  Result := Dialog;
end;

{ TDialog }

procedure TDialog.VisualRequired;
begin
  if not Assigned(DialogExecuteProc) then
    raise Exception.Create('Nenhum form de di�logo associado (Uses Vcl.InputFoms)');
end;

constructor TDialog.Create;
begin
  FFields := TObjectList<TField>.Create;
end;

destructor TDialog.Destroy;
begin
  FFields.Free;
  inherited;
end;

procedure TDialog.Execute;
begin
  VisualRequired;
  //Todo: Implementar "guardar os valores originais"
  DialogExecuteProc(Self);
end;

function TDialog.GetWindowTitle: string;
begin
  Result := FWindowTitle;
end;

procedure TDialog.SetWindowTitle(const ATitle: string);
begin
  FWindowTitle := ATitle;
end;

function TDialog.Fields: TObjectList<TField>;
begin
  Result := FFields;
end;

{ TField }

function TField.GetKind: TTypeKind;
begin
  Result := Kind;
end;

function TField.GetReference: Pointer;
begin
  Result := Reference;
end;

procedure TField.Save;
begin
  if Assigned(DialogSaveProc) then
    DialogSaveProc(Self);
end;

end.
